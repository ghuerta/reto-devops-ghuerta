FROM nginx:latest
COPY ./nginx-certificate.crt /etc/nginx/certificate/nginx-certificate.crt
COPY ./nginx.key /etc/nginx/certificate/nginx.key
COPY ./default.conf /etc/nginx/conf.d/default.conf
COPY ./.htpasswd /etc/nginx/conf.d/.htpasswd