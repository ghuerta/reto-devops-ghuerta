FROM node:alpine

#Creacion de carpeta de la aplicacion
RUN mkdir app

#Selecionar carpeta de trabajo
WORKDIR /app

#Copiar el package.json y package-lock.json
COPY package*.json /app/

#Instalar dependencias
RUN npm install

#Copia de recurso de app
COPY . .

#Cambio de usuario root a node
USER node

EXPOSE 3000

CMD [ "node","index.js" ]